package uz.myapps.uzmobileussd.models

import java.io.Serializable

class Aloqa:Serializable {
    var name: String? = null
    var title: String? = null

    constructor()
    constructor(name: String?, title: String?) {
        this.name = name
        this.title = title
    }
}