package uz.myapps.uzmobileussd.models

import java.io.Serializable

class Paket : Serializable {
    var name: String? = null
    var amount: Int? = null
    var price: String? = null
    var time: String? = null
    var kind: String? = null
    var activeCod: String? = null

    constructor()
    constructor(
        name: String?,
        amount: Int?,
        price: String?,
        time: String?,
        kind: String?,
        activeCod: String?
    ) {
        this.name = name
        this.amount = amount
        this.price = price
        this.time = time
        this.kind = kind
        this.activeCod = activeCod
    }
}