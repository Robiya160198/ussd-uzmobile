package uz.myapps.uzmobileussd.models

class Xizmat {
    var name: String? = null
    var about: String? = null
    var activeCod: String? = null
    var passivCod: String? = null

    constructor()
    constructor(name: String?, about: String?, activeCod: String?) {
        this.name = name
        this.about = about
        this.activeCod = activeCod
    }

    constructor(name: String?, about: String?, activeCod: String?, passivCod: String?) {
        this.name = name
        this.about = about
        this.activeCod = activeCod
        this.passivCod = passivCod
    }

}