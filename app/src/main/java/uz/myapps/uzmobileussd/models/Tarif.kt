package uz.myapps.uzmobileussd.models

import java.io.Serializable

class Tarif : Serializable {
    var name: String? = null
    var minuts: String? = null
    var mb: String? = null
    var sms: String? = null
    var money: String? = null
    var about: String? = null
    var activeCod: String? = null

    constructor(name: String?, minuts: String?, mb: String?, sms: String?, money: String?) {
        this.name = name
        this.minuts = minuts
        this.mb = mb
        this.sms = sms
        this.money = money
    }

    constructor()
    constructor(
        name: String?,
        minuts: String?,
        mb: String?,
        sms: String?,
        money: String?,
        about: String?
    ) {
        this.name = name
        this.minuts = minuts
        this.mb = mb
        this.sms = sms
        this.money = money
        this.about = about
    }
}