package uz.myapps.uzmobileussd.models

class News {
    var name: String? = null
    var about: String? = null
    var time: String? = null

    constructor()
    constructor(name: String?, about: String?, time: String?) {
        this.name = name
        this.about = about
        this.time = time
    }
}