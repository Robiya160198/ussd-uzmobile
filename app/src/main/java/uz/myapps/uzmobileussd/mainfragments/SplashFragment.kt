package uz.myapps.uzmobileussd.mainfragments

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.preference.PreferenceManager
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import uz.myapps.uzmobileussd.R
import uz.myapps.uzmobileussd.USSDActivity
import uz.myapps.uzmobileussd.localehelper.LocaleHelper
import java.util.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [SplashFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class SplashFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val inflate = inflater.inflate(R.layout.fragment_splash, container, false)
        val preference =
            PreferenceManager.getDefaultSharedPreferences(requireContext())
        val lang = preference.getString("Language", "null")
        if (lang != "null") {
            LocaleHelper().setLocale(requireContext(), lang)
            Handler().postDelayed({
                val intent = Intent(
                    requireContext(),
                    USSDActivity::class.java
                )
                startActivity(intent)
                requireActivity().finish()
            }, 3000)
        } else
            Handler().postDelayed({
                fragmentManager?.beginTransaction()?.replace(
                    R.id.container,
                    ChooseLanguageFragment()
                )?.commit()
            }, 3000)
        return inflate
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

    }

    companion object {

        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            SplashFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}