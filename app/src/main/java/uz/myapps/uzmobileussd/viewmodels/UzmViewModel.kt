package uz.myapps.uzmobileussd.viewmodels

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import uz.myapps.uzmobileussd.models.*
import uz.myapps.uzmobileussd.repository.UzmRepository

class UzmViewModel : ViewModel() {

    val repository = UzmRepository()

    fun getXizmatData(context: Context): MutableLiveData<List<Xizmat>> {
        return repository.getXizmatList(context)
    }
    fun getUSSDData(context: Context): MutableLiveData<List<Xizmat>> {
        return repository.getUSSDList(context)
    }
    fun getTarifData(context: Context): MutableLiveData<List<Tarif>> {
        return repository.getTarifList(context)
    }
    fun getAloqaData(context: Context): MutableLiveData<List<Aloqa>> {
        return repository.getAloqaList(context)
    }
    fun getNewsData(context: Context): MutableLiveData<List<News>> {
        return repository.getNewsList(context)
    }
    fun getPaketData(param1: String?,context: Context): MutableLiveData<List<Paket>> {
        return repository.getPaketList(param1,context)
    }
    fun getPaketNetData(param1: String?,context: Context): MutableLiveData<List<Paket>> {
        return repository.getPaketNetList(param1,context)
    }
    fun getPaketMinutData(param1: String?,context: Context): MutableLiveData<List<Paket>> {
        return repository.getPaketMinutList(param1,context)
    }
}