package uz.myapps.uzmobileussd.repository

import android.content.ContentValues.TAG
import android.content.Context
import android.preference.PreferenceManager
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.firebase.database.*
import uz.myapps.uzmobileussd.models.*

class UzmRepository {

    fun getXizmatList(context: Context): MutableLiveData<List<Xizmat>> {
        var list = MutableLiveData<List<Xizmat>>()
        var tempList = ArrayList<Xizmat>()
        val database = FirebaseDatabase.getInstance()
        val preference =
            PreferenceManager.getDefaultSharedPreferences(context)
        val lang = preference.getString("Language", "null")
        var myRef:DatabaseReference
        when (lang) {
            "uz-rUZ" -> {
                myRef = database.getReference("xizmatlar")
            }
            "ru"->{
                myRef = database.getReference("xizmatlar_ru")
            }
            else ->{
                myRef = database.getReference("xizmatlar_uz")
            }
        }
        myRef.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                Log.w(TAG, "Failed to read value.", error.toException());
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                var value = snapshot.children
                for (child in value) {
                    val value1 = child.getValue(Xizmat::class.java)
                    if (value1 != null)
                        tempList.add(value1)
                }
                list.value = tempList
            }
        })
        return list
    }

    fun getUSSDList(context: Context): MutableLiveData<List<Xizmat>> {
        var list = MutableLiveData<List<Xizmat>>()
        var tempList = ArrayList<Xizmat>()
        val database = FirebaseDatabase.getInstance()
        val preference =
            PreferenceManager.getDefaultSharedPreferences(context)
        val lang = preference.getString("Language", "null")
        var myRef:DatabaseReference
        when (lang) {
            "uz-rUZ" -> {
                myRef = database.getReference("USSDkodlar")
            }
            "ru"->{
                myRef = database.getReference("USSDkodlar_ru")
            }
            else ->{
                myRef = database.getReference("USSDkodlar_uz")
            }
        }
        myRef.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                Log.w(TAG, "Failed to read value.", error.toException());
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                var value = snapshot.children
                for (child in value) {
                    val value1 = child.getValue(Xizmat::class.java)
                    if (value1 != null)
                        tempList.add(value1)

                }
                list.value = tempList

            }
        })
        return list
    }

    fun getTarifList(context: Context): MutableLiveData<List<Tarif>> {
        var list = MutableLiveData<List<Tarif>>()
        var tempList = ArrayList<Tarif>()
        val database = FirebaseDatabase.getInstance()
        val preference =
            PreferenceManager.getDefaultSharedPreferences(context)
        val lang = preference.getString("Language", "null")
        var myRef:DatabaseReference
        when (lang) {
            "uz-rUZ" -> {
                myRef = database.getReference("tariflar")
            }
            "ru"->{
                myRef = database.getReference("tariflar_ru")
            }
            else ->{
                myRef = database.getReference("tariflar_uz")
            }
        }
        myRef.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                Log.w(TAG, "Failed to read value.", error.toException());
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                var value = snapshot.children
                for (child in value) {
                    val value1 = child.getValue(Tarif::class.java)
                    if (value1 != null)
                        tempList.add(value1)
                }
                list.value = tempList
            }
        })
        return list
    }

    fun getAloqaList(context: Context): MutableLiveData<List<Aloqa>> {
        var list = MutableLiveData<List<Aloqa>>()
        var tempList = ArrayList<Aloqa>()
        val database = FirebaseDatabase.getInstance()
        val preference =
            PreferenceManager.getDefaultSharedPreferences(context)
        val lang = preference.getString("Language", "null")
        var myRef:DatabaseReference
        when (lang) {
            "uz-rUZ" -> {
                myRef = database.getReference("operator")
            }
            "ru"->{
                myRef = database.getReference("operator_ru")
            }
            else ->{
                myRef = database.getReference("operator_uz")
            }
        }

        myRef.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                Log.w(TAG, "Failed to read value.", error.toException());
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                var value = snapshot.children
                for (child in value) {
                    val value1 = child.getValue(Aloqa::class.java)
                    if (value1 != null)
                        tempList.add(value1)
                }
                list.value = tempList
            }
        })
        return list
    }

    fun getNewsList(context: Context): MutableLiveData<List<News>> {
        var list = MutableLiveData<List<News>>()
        var tempList = ArrayList<News>()
        val database = FirebaseDatabase.getInstance()
        val preference =
            PreferenceManager.getDefaultSharedPreferences(context)
        val lang = preference.getString("Language", "null")
        var myRef:DatabaseReference
        when (lang) {
            "uz-rUZ" -> {
                myRef = database.getReference("yangiliklar")
            }
            "ru"->{
                myRef = database.getReference("yangilik_ru")
            }
            else ->{
                myRef = database.getReference("yangilik_uz")
            }
        }
        myRef.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                Log.w(TAG, "Failed to read value.", error.toException());
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                var value = snapshot.children
                for (child in value) {
                    val value1 = child.getValue(News::class.java)
                    if (value1 != null)
                        tempList.add(value1)
                }
                list.value = tempList
            }
        })
        return list
    }

    fun getPaketList(param1: String?, context: Context): MutableLiveData<List<Paket>> {
        var list = MutableLiveData<List<Paket>>()
        var tempList = ArrayList<Paket>()
        val database = FirebaseDatabase.getInstance()
        val preference =
            PreferenceManager.getDefaultSharedPreferences(context)
        val lang = preference.getString("Language", "null")
        var myRef:DatabaseReference
        when (lang) {
            "uz-rUZ" -> {
                myRef = database.getReference("sms")
            }
            "ru"->{
                myRef = database.getReference("sms_ru")
            }
            else ->{
                myRef = database.getReference("sms_uz")
            }
        }
        myRef.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                Log.w(TAG, "Failed to read value.", error.toException());
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                var value = snapshot.children
                for (child in value) {
                    val value1 = child.getValue(Paket::class.java)
                    if (value1 != null && value1.kind == param1)
                        tempList.add(value1)
                }
                list.value = tempList
            }
        })
        return list
    }

    fun getPaketNetList(param1: String?, context: Context): MutableLiveData<List<Paket>> {
        val database = FirebaseDatabase.getInstance()
        val preference =
            PreferenceManager.getDefaultSharedPreferences(context)
        val lang = preference.getString("Language", "null")
        var myRef:DatabaseReference
        when (lang) {
            "uz-rUZ" -> {
                myRef = database.getReference("internet")
            }
            "ru"->{
                myRef = database.getReference("internet_ru")
            }
            else ->{
                myRef = database.getReference("internet_uz")
            }
        }
        var list = MutableLiveData<List<Paket>>()
        var tempList = ArrayList<Paket>()
        myRef.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                Log.w(TAG, "Failed to read value.", error.toException());
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                var value = snapshot.children
                for (child in value) {
                    val value1 = child.getValue(Paket::class.java)
                    if (value1 != null && value1.kind == param1)
                        tempList.add(value1)
                }
                list.value = tempList
            }
        })
        return list
    }

    fun getPaketMinutList(param1: String?, context: Context): MutableLiveData<List<Paket>> {
        var list = MutableLiveData<List<Paket>>()
        var tempList = ArrayList<Paket>()
        val database = FirebaseDatabase.getInstance()
        val preference =
            PreferenceManager.getDefaultSharedPreferences(context)
        val lang = preference.getString("Language", "null")
        var myRef:DatabaseReference
        when (lang) {
            "uz-rUZ" -> {
                myRef = database.getReference("minutlar")
            }
            "ru"->{
                myRef = database.getReference("minutlar_ru")
            }
            else ->{
                myRef = database.getReference("minutlar_uz")
            }
        }
        myRef.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                Log.w(TAG, "Failed to read value.", error.toException());
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                var value = snapshot.children
                for (child in value) {
                    val value1 = child.getValue(Paket::class.java)
                    if (value1 != null && value1.kind == param1)
                        tempList.add(value1)
                }
                list.value = tempList
            }
        })
        return list
    }
}