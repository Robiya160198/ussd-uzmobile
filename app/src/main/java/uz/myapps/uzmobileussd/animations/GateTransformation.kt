package uz.myapps.uzmobileussd.animations

import android.view.View
import androidx.viewpager.widget.ViewPager.PageTransformer


class GateTransformation : PageTransformer {
    private val TAG = "GateAnimationn"
    override fun transformPage(page: View, position: Float) {
        page.setTranslationX(-position * page.getWidth())
        if (position < -1) {
            page.setAlpha(0f)
        } else if (position <= 0) {    // [-1,0]
            page.setAlpha(1f)
            page.setPivotX(0f)
            page.setRotationY(90 * Math.abs(position))
        } else if (position <= 1) {    // (0,1]
            page.setAlpha(1f)
            page.setPivotX(page.getWidth().toFloat())
            page.setRotationY(-90 * Math.abs(position))
        } else {
            page.setAlpha(0f)
        }
    }
}