package uz.myapps.uzmobileussd.animations

import android.view.View
import androidx.viewpager.widget.ViewPager.PageTransformer


class VerticalFlipTransformation : PageTransformer {
    override fun transformPage(page: View, position: Float) {
        page.setTranslationX(-position * page.getWidth())
        page.setCameraDistance(12000f)
        if (position < 0.5 && position > -0.5) {
            page.setVisibility(View.VISIBLE)
        } else {
            page.setVisibility(View.INVISIBLE)
        }
        if (position < -1) {     // [-Infinity,-1)
            // This page is way off-screen to the left.
            page.setAlpha(0f)
        } else if (position <= 0) {    // [-1,0]
            page.setAlpha(1f)
            page.setRotationY(180 * (1 - Math.abs(position) + 1))
        } else if (position <= 1) {    // (0,1]
            page.setAlpha(1f)
            page.setRotationY(-180 * (1 - Math.abs(position) + 1))
        } else {    // (1,+Infinity]
            // This page is way off-screen to the right.
            page.setAlpha(0f)
        }
    }
}