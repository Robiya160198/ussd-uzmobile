package uz.myapps.uzmobileussd

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.app.ShareCompat
import androidx.core.content.ContextCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.NavigationUI.*
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_u_s_s_d.*
import uz.myapps.uzmobileussd.ussd_fragments.HomeFragment
import uz.myapps.uzmobileussd.ussd_fragments.NewsFragment
import uz.myapps.uzmobileussd.ussd_fragments.OperatorFragment
import java.net.URLEncoder
import java.util.*

class USSDActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {
    companion object {
        lateinit var mdrawerLayout: DrawerLayout
    }

    lateinit var actionBarDrawerToggle: ActionBarDrawerToggle
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_u_s_s_d)
        actionBarDrawerToggle =
            ActionBarDrawerToggle(this, drawer_layout, R.string.open, R.string.close)
        drawer_layout.addDrawerListener(actionBarDrawerToggle)
        actionBarDrawerToggle.syncState()
        mdrawerLayout = findViewById(R.id.drawer_layout)
        bottom_navview.setOnNavigationItemSelectedListener(this)
        nav_view.setNavigationItemSelectedListener { p0 ->
            when (p0.itemId) {
                R.id.nav_telegram -> {
                    var gourl = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.t.me/ussduz"))
                    startActivity(gourl)
                    return@setNavigationItemSelectedListener true
                }
                R.id.nav_aloqa -> {
                    val builder = AlertDialog.Builder(this)
                    builder.setTitle("Biz bilan aloqa")
                    builder.setMessage("Email:farmonovarobiya@gmail.com")
                    builder.setPositiveButton("xat jo'natish") { a, b ->
                        val emailIntent = Intent(
                            Intent.ACTION_SENDTO, Uri.fromParts(
                                "mailto", "farmonovarobiya@gmail.com", null
                            )
                        )
                        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject")
                        emailIntent.putExtra(Intent.EXTRA_TEXT, "Body")
                        startActivity(Intent.createChooser(emailIntent, "Send email..."))
                    }
                    builder.create().show()
                    return@setNavigationItemSelectedListener true
                }
                R.id.nav_ulashish -> {
                    ShareCompat.IntentBuilder.from(this)
                        .setType("text/plain")
                        .setChooserTitle("Chooser title")
                        .setText("Agar programmamni play marketga qo'ysam albatta yuklab ol")
                        .startChooser()
                    return@setNavigationItemSelectedListener true
                }
                R.id.nav_baholash -> {
                    var gourl = Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("https://play.google.com/store/apps/details?id=uz.pdp.uzmobile")
                    )
                    startActivity(gourl)
                    return@setNavigationItemSelectedListener true
                }
                else -> {
                    val builder = AlertDialog.Builder(this)
                    builder.setTitle("Biz haqimizda")
                    builder.setMessage("Biz kim mulki Turon, amiri Turkistonmiz")
                    builder.create().show()
                    return@setNavigationItemSelectedListener true
                }
            }
        }

    }

    override fun onSupportNavigateUp(): Boolean {
        return Navigation.findNavController(this, R.id.nav_graph).navigateUp()
    }

    override fun onNavigationItemSelected(p0: MenuItem): Boolean {
        when (p0.itemId) {
            R.id.news -> {
                supportFragmentManager.beginTransaction().replace(
                    R.id.nav_host_fragment,
                    NewsFragment()
                ).addToBackStack("").commit()
                return true
            }
            R.id.operator -> {
                supportFragmentManager.beginTransaction().replace(
                    R.id.nav_host_fragment,
                    OperatorFragment()
                ).addToBackStack("").commit()
                return true
            }
            R.id.balans -> {
                setupPermissions()
                return true
            }
            R.id.settings -> {
                val intent = Intent(this, MainActivity::class.java)
                intent.putExtra("isReturn", true)
                startActivity(intent)
                finish()
                return true
            }
            else -> {
                supportFragmentManager.beginTransaction().replace(
                    R.id.nav_host_fragment,
                    HomeFragment()
                ).addToBackStack("").commit()
                return true
            }
        }
    }

    private fun setupPermissions() {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.CALL_PHONE
            )
            != PackageManager.PERMISSION_GRANTED
        ) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this,
                    Manifest.permission.CALL_PHONE
                )
            ) {
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.CALL_PHONE),
                    42
                )
            }
        } else {
            // Permission has already been granted
            callPhone()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 42) {
            // If request is cancelled, the result arrays are empty.
            if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                // permission was granted, yay!
                callPhone()
            } else {
            }
            return
        }
    }

    private fun callPhone() {
        val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel:" + URLEncoder.encode("*107#")))
        startActivity(intent)
    }
}