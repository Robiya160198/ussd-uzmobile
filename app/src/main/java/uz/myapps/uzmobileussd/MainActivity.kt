package uz.myapps.uzmobileussd

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import uz.myapps.uzmobileussd.mainfragments.ChooseLanguageFragment
import uz.myapps.uzmobileussd.mainfragments.SplashFragment

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val isReturn = intent.getBooleanExtra("isReturn", false)
        if (isReturn)
            supportFragmentManager.beginTransaction().add(
                R.id.container,
                ChooseLanguageFragment()
            ).commit()
        else
            supportFragmentManager.beginTransaction().add(
                R.id.container,
                SplashFragment()
            ).commit()

    }
}