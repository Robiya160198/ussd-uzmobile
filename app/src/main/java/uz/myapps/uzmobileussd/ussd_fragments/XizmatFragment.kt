package uz.myapps.uzmobileussd.ussd_fragments

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ExpandableListView
import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.fragment_xizmat.view.*
import kotlinx.android.synthetic.main.item_child.*
import uz.myapps.uzmobileussd.R
import uz.myapps.uzmobileussd.adapters.ExpandableAdapter
import uz.myapps.uzmobileussd.models.Xizmat
import uz.myapps.uzmobileussd.viewmodels.UzmViewModel
import java.net.URLEncoder


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [XizmatFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class XizmatFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var list = ArrayList<Xizmat>()
    private val TAG = "XizmatFragment"
    private lateinit var adapter: ExpandableAdapter
    private lateinit var ev: ExpandableListView
    private lateinit var activeCod: String
    lateinit var uzmViewModel: UzmViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val inflate = inflater.inflate(R.layout.fragment_xizmat, container, false)
        ev = inflate.findViewById<ExpandableListView>(R.id.expand_xizmat)
        if (list.size == 0) {
            getData()
        }
        ev.setOnGroupExpandListener(object : ExpandableListView.OnGroupExpandListener {
            var previousGroup: Int = -1
            override fun onGroupExpand(p0: Int) {
                if (p0 != previousGroup)
                    ev.collapseGroup(previousGroup);
                previousGroup = p0;
            }
        })
        val metrics = DisplayMetrics()
        requireActivity().windowManager.getDefaultDisplay().getMetrics(metrics)
        val width = metrics.widthPixels
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) {
            ev.setIndicatorBounds(
                width - GetPixelFromDips(50f),
                width - GetPixelFromDips(10f)
            )

        } else {
            ev.setIndicatorBoundsRelative(
                width - GetPixelFromDips(50f),
                width - GetPixelFromDips(10f)
            )
        }
        inflate.back_xizmat.setOnClickListener {
            activity?.onBackPressed()
        }
        return inflate
    }

    private fun getData() {
        uzmViewModel = ViewModelProviders.of(this)[UzmViewModel::class.java]
        uzmViewModel.getXizmatData(requireContext()).observe(requireActivity(), Observer {
            adapter = ExpandableAdapter(requireContext(), it, object :
                ExpandableAdapter.Ulanish {
                override fun Ulanish(cod: String) {
                    activeCod = cod
                    setupPermissions()
                }
            })
            ev.setAdapter(adapter)
        })

    }

    private fun setupPermissions() {
        if (ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.CALL_PHONE
            )
            != PackageManager.PERMISSION_GRANTED
        ) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    requireActivity(),
                    Manifest.permission.CALL_PHONE
                )
            ) {
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(
                    requireActivity(),
                    arrayOf(Manifest.permission.CALL_PHONE),
                    42
                )
            }
        } else {
            // Permission has already been granted
            callPhone()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 42) {
            // If request is cancelled, the result arrays are empty.
            if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                // permission was granted, yay!
                callPhone()
            } else {
            }
            return
        }
    }

    private fun callPhone() {
        val intent =
            Intent(Intent.ACTION_CALL, Uri.parse("tel:" + URLEncoder.encode("${activeCod}")))
        startActivity(intent)
    }

    fun GetPixelFromDips(pixels: Float): Int {
        // Get the screen's density scale
        val scale = resources.displayMetrics.density
        // Convert the dps to pixels, based on density scale
        return (pixels * scale + 0.5f).toInt()
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment XizmatFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            XizmatFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}