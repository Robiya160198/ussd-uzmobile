package uz.myapps.uzmobileussd.ussd_fragments

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_tarif.view.*
import uz.myapps.uzmobileussd.R
import uz.myapps.uzmobileussd.adapters.RvTarifAdapter
import uz.myapps.uzmobileussd.models.Tarif
import uz.myapps.uzmobileussd.viewmodels.UzmViewModel
import java.net.URLEncoder

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [TarifFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class TarifFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    lateinit var adapter: RvTarifAdapter
    private val TAG = "TarifFragment"
    private var list = ArrayList<Tarif>()
    lateinit var uzmViewModel: UzmViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val inflate = inflater.inflate(R.layout.fragment_tarif, container, false)
        val rv = inflate.findViewById<RecyclerView>(R.id.rv_tarif)
        uzmViewModel = ViewModelProviders.of(this)[UzmViewModel::class.java]
        uzmViewModel.getTarifData(requireContext()).observe(requireActivity(), Observer {
            adapter = RvTarifAdapter(it,object :RvTarifAdapter.onClick{
                override fun onclick(position: Int) {
                    val bundleOf = bundleOf("position" to position+1)
                    Navigation.findNavController(inflate).navigate(R.id.action_tarifFragment2_to_bannerFragment,bundleOf)
                }
            })
            rv.adapter = adapter
        })

        inflate.back_tarif.setOnClickListener {
            activity?.onBackPressed()
        }
        inflate.checkup_tarif.setOnClickListener {
            setupPermissions()
        }
        return inflate
    }
    private fun setupPermissions() {
        if (ContextCompat.checkSelfPermission(requireContext(),
                Manifest.permission.CALL_PHONE)
            != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(requireActivity(),
                    Manifest.permission.CALL_PHONE)) {
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(requireActivity(),
                    arrayOf(Manifest.permission.CALL_PHONE),
                    42)
            }
        } else {
            // Permission has already been granted
            callPhone()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 42) {
            // If request is cancelled, the result arrays are empty.
            if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                // permission was granted, yay!
                callPhone()
            } else {
            }
            return
        }
    }
    private fun callPhone() {
        val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel:" + URLEncoder.encode("*107#")))
        startActivity(intent)
    }
    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment TarifFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            TarifFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}