package uz.myapps.uzmobileussd.ussd_fragments.viewpagerfragments

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_page_view_banner.view.*
import uz.myapps.uzmobileussd.R
import uz.myapps.uzmobileussd.models.Tarif
import java.net.URLEncoder

private const val ARG_PARAM1 = "param1"

class PageViewBannerFragment : Fragment() {
    private var param1: Tarif? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getSerializable(ARG_PARAM1) as Tarif?
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_page_view_banner, container, false)
        root.name_tarif.text = param1?.name
        root.how_mbs.text = param1?.mb
        root.how_minuts.text = param1?.minuts
        root.how_money.text = param1?.money
        root.how_sms.text = param1?.sms
        root.tarif_10_.text = "Tarif \"${param1?.name}\""
        root.abonent_tolovi.text = param1?.money?.substring(7)
        root.tarif_rejas.text = param1?.about
        root.btn_more.setOnClickListener {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("https://uztelecom.uz/uz/jismoniy-shaxslarga/mobil-aloqa/gsm-2/tariflar")
                )
            )
        }
        root.btn_ulanish.setOnClickListener {
            if(param1?.activeCod!=null)
            setupPermissions()
            else Toast.makeText(requireContext(), "Ushbu tarifga faqatgina yangi abonentlar uchun amal qiladi", Toast.LENGTH_SHORT).show()
        }
        return root
    }

    private fun setupPermissions() {
        if (ContextCompat.checkSelfPermission(requireContext(),
                Manifest.permission.CALL_PHONE)
            != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(requireActivity(),
                    Manifest.permission.CALL_PHONE)) {
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(requireActivity(),
                    arrayOf(Manifest.permission.CALL_PHONE),
                    42)
            }
        } else {
            // Permission has already been granted
            callPhone()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 42) {
            // If request is cancelled, the result arrays are empty.
            if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                // permission was granted, yay!
                callPhone()
            } else {
            }
            return
        }
    }
    private fun callPhone() {
        val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel:" + URLEncoder.encode("${param1?.activeCod}")))
        startActivity(intent)
    }

    companion object {

        @JvmStatic
        fun newInstance(param1: Tarif) =
            PageViewBannerFragment()
                .apply {
                    arguments = Bundle().apply {
                        putSerializable(ARG_PARAM1, param1)
                    }
                }
    }
}