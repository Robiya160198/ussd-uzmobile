package uz.myapps.uzmobileussd.ussd_fragments.viewpagerfragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.database.FirebaseDatabase
import uz.myapps.uzmobileussd.R
import uz.myapps.uzmobileussd.adapters.RvAdapter
import uz.myapps.uzmobileussd.models.Paket
import uz.myapps.uzmobileussd.viewmodels.UzmViewModel

private const val ARG_PARAM1 = "param1"
class PageViewMinutFragment : Fragment() {
    private var param1: String? = null
    lateinit var adapter: RvAdapter
    private val TAG = "PageViewMinutFragment"
    private var list = ArrayList<Paket>()
    private lateinit var uzmViewModel: UzmViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val inflate = inflater.inflate(R.layout.fragment_page_view_minut, container, false)
        val rv = inflate.findViewById<RecyclerView>(R.id.rv_minut)
        uzmViewModel=ViewModelProviders.of(this)[UzmViewModel::class.java]
        uzmViewModel.getPaketMinutData(param1,requireContext()).observe(requireActivity(), Observer {
            adapter = RvAdapter(it)
            rv.adapter = adapter
        })
        return inflate
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String) =
            PageViewMinutFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                }
            }
    }
}