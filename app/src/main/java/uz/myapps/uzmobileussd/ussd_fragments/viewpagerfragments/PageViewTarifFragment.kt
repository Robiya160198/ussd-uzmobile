package uz.myapps.uzmobileussd.ussd_fragments.viewpagerfragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_pageview_tarif.view.*
import uz.myapps.uzmobileussd.R
import uz.myapps.uzmobileussd.adapters.TarifPageViewAdapter
import uz.myapps.uzmobileussd.models.Tarif


private const val ARG_PARAM1 = "param1"
private lateinit var onclick: TarifPageViewAdapter.onClick

class PageViewTarifFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: Tarif? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getSerializable(ARG_PARAM1) as Tarif
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_pageview_tarif, container, false)
        root.name_tarif.text = param1?.name
        root.how_mbs.text = param1?.mb
        root.how_minuts.text = param1?.minuts
        root.how_money.text = param1?.money
        root.how_sms.text = param1?.sms
        root.rlayout.setOnClickListener {
            onclick.onclick()
        }
        return root
    }

    companion object {
        @JvmStatic
        fun newInstance(
            param1: Tarif,
            param: TarifPageViewAdapter.onClick
        ):Fragment {
            onclick = param
            return PageViewTarifFragment()
                .apply {
                arguments = Bundle().apply {
                    putSerializable(ARG_PARAM1, param1)
                }
            }
        }
    }
}