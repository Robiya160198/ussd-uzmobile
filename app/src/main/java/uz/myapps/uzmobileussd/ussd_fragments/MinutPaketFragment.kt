package uz.myapps.uzmobileussd.ussd_fragments

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.preference.PreferenceManager
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.fragment_internet_paket.view.*
import kotlinx.android.synthetic.main.fragment_minut_paket.view.*
import uz.myapps.uzmobileussd.R
import uz.myapps.uzmobileussd.adapters.MinutPageViewAdapter
import uz.myapps.uzmobileussd.adapters.NetPageViewAdapter
import java.net.URLEncoder

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
class MinutPaketFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var list = ArrayList<String>()
    var lang:String?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val inflate = inflater.inflate(R.layout.fragment_minut_paket, container, false)
        val preference =
            PreferenceManager.getDefaultSharedPreferences(requireContext())
        lang = preference.getString("Language", "null")
        load()
        val adapter = MinutPageViewAdapter(list, requireFragmentManager())
        inflate.view_pager_minut.adapter = adapter
        inflate.tablayout_minut.setupWithViewPager(inflate.view_pager_minut)
        setupTabs(inflate)
        val tabLayout = inflate.findViewById(R.id.tablayout_minut) as TabLayout
        val betweenSpace = 30

        val slidingTabStrip = tabLayout.getChildAt(0) as ViewGroup

        for (i in 0 until slidingTabStrip.childCount - 1) {
            val v = slidingTabStrip.getChildAt(i)
            val params = v.layoutParams as ViewGroup.MarginLayoutParams
            params.rightMargin = betweenSpace
        }
        inflate.back_minut.setOnClickListener {
            activity?.onBackPressed()
        }
        inflate.checkup_minut.setOnClickListener {
            setupPermissions()
        }
        return inflate
    }
    private fun setupPermissions() {
        if (ContextCompat.checkSelfPermission(requireContext(),
                Manifest.permission.CALL_PHONE)
            != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(requireActivity(),
                    Manifest.permission.CALL_PHONE)) {
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(requireActivity(),
                    arrayOf(Manifest.permission.CALL_PHONE),
                    42)
            }
        } else {
            // Permission has already been granted
            callPhone()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 42) {
            // If request is cancelled, the result arrays are empty.
            if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                // permission was granted, yay!
                callPhone()
            } else {
            }
            return
        }
    }
    private fun callPhone() {
        val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel:" + URLEncoder.encode("*107#")))
        startActivity(intent)
    }
    private fun setupTabs(
        view: View
    ) {
        val childCount = view.tablayout_minut.tabCount
        for (i in 0 until childCount) {
            val tab = view.tablayout_minut.getTabAt(i)
            val tabView = layoutInflater.inflate(
                R.layout.tab_item,
                null,
                false
            )
            val tv = tabView.findViewById<TextView>(R.id.tv)
            val linear = tabView.findViewById<LinearLayout>(R.id.linear)
            tv.text = list[i]
            tab?.customView = tabView
        }
    }

    fun load() {
        when(lang){
            "ru"->{
                list.add("Минутные пакеты")
                list.add("Полезная замена")
            }
            "uz"->{
                list.add("Дақиқа тўпламлар")
                list.add("Фойдали алмаштирув")
            }
            else->{
                list.add("Daqiqa to'plamlar")
                list.add("Foydali almashtiruv")
            }
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String) =
            MinutPaketFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                }
            }
    }
}