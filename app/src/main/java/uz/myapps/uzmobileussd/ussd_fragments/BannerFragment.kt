package uz.myapps.uzmobileussd.ussd_fragments

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.fragment_banner.view.*
import kotlinx.android.synthetic.main.fragment_home.view.*
import uz.myapps.uzmobileussd.R
import uz.myapps.uzmobileussd.adapters.BannerPageViewAdapter
import uz.myapps.uzmobileussd.adapters.TarifPageViewAdapter
import uz.myapps.uzmobileussd.animations.GateTransformation
import uz.myapps.uzmobileussd.models.Tarif
import uz.myapps.uzmobileussd.viewmodels.UzmViewModel

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"

class BannerFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: Int? = null
    lateinit var adapter: BannerPageViewAdapter
    private lateinit var uzmViewModel: UzmViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getInt(ARG_PARAM1)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        uzmViewModel = ViewModelProviders.of(this)[UzmViewModel::class.java]
        uzmViewModel.getTarifData(requireContext()).observe(requireActivity(), Observer {
            adapter = BannerPageViewAdapter(it, requireFragmentManager())
            view.view_pager_banner.adapter = adapter
            view.view_pager_banner.setCurrentItem(arguments?.getInt("position")!! - 1)
            view.progressBar.visibility = View.GONE
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val inflate = inflater.inflate(R.layout.fragment_banner, container, false)
        inflate.progressBar.visibility = View.VISIBLE
//        val gateTransformation = GateTransformation()
//        inflate.view_pager_banner.setPageTransformer(true,gateTransformation)
        uzmViewModel = ViewModelProviders.of(this)[UzmViewModel::class.java]
        uzmViewModel.getTarifData(requireContext()).observe(requireActivity(), Observer {
            adapter = BannerPageViewAdapter(it, requireFragmentManager())
            inflate.view_pager_banner.adapter = adapter
            inflate.view_pager_banner.setCurrentItem(arguments?.getInt("position")!! - 1)
            inflate.progressBar.visibility = View.GONE
        })

        inflate.back_banner.setOnClickListener {
            activity?.onBackPressed()
        }
        return inflate
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment BannerFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: Int) =
            BannerFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_PARAM1, param1)
                }
            }
    }
}