package uz.myapps.uzmobileussd.ussd_fragments

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.core.app.ShareCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavOptions
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import com.google.android.material.tabs.TabLayout
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.fragment_banner.view.*
import kotlinx.android.synthetic.main.fragment_home.view.*
import uz.myapps.uzmobileussd.BuildConfig
import uz.myapps.uzmobileussd.R
import uz.myapps.uzmobileussd.USSDActivity
import uz.myapps.uzmobileussd.adapters.RvTarifAdapter
import uz.myapps.uzmobileussd.adapters.TarifPageViewAdapter
import uz.myapps.uzmobileussd.animations.GateTransformation
import uz.myapps.uzmobileussd.animations.VerticalFlipTransformation
import uz.myapps.uzmobileussd.models.Tarif
import uz.myapps.uzmobileussd.viewmodels.UzmViewModel


private const val ARG_PARAM1 = "param1"

class HomeFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    lateinit var adapter: TarifPageViewAdapter
    private val TAG = "HomeFragment"
    private var list = ArrayList<Tarif>()
    lateinit var uzmViewModel: UzmViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        uzmViewModel = ViewModelProviders.of(this)[UzmViewModel::class.java]
        uzmViewModel.getTarifData(requireContext()).observe(requireActivity(), Observer {
            adapter = TarifPageViewAdapter(view, it, requireFragmentManager())
            view.view_pager.adapter = adapter
            view.linear_bar.visibility=View.GONE
        })
    }
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val inflate = inflater.inflate(R.layout.fragment_home, container, false)
        inflate.linear_bar.visibility=View.VISIBLE
        val gateTransformation = VerticalFlipTransformation()
        inflate.view_pager.setPageTransformer(true,gateTransformation)
        uzmViewModel = ViewModelProviders.of(this)[UzmViewModel::class.java]
        uzmViewModel.getTarifData(requireContext()).observe(requireActivity(), Observer {
            adapter = TarifPageViewAdapter(inflate, it, requireFragmentManager())
            inflate.view_pager.adapter = adapter
            inflate.linear_bar.visibility=View.GONE
        })
        inflate.tablayout.setupWithViewPager(inflate.view_pager)
        inflate.share.setOnClickListener {
            ShareCompat.IntentBuilder.from(requireActivity())
                .setType("text/plain")
                .setChooserTitle("Chooser title")
                .setText("Agar programmamni play marketga qo'ysam albatta yuklab ol")
                .startChooser();
        }
        inflate.telegram.setOnClickListener {
            var gourl= Intent(Intent.ACTION_VIEW, Uri.parse("https://www.t.me/ussduz"))
            startActivity(gourl)
        }
        inflate.menu.setOnClickListener {
            USSDActivity.mdrawerLayout.openDrawer(Gravity.LEFT)
        }
        inflate.btn_net.setOnClickListener {
       //     val build = NavOptions.Builder().setPopUpTo(R.id.homeFragment, true).build()
            Navigation.findNavController(inflate).navigate(R.id.action_homeFragment_to_internetPaketFragment)
        }
        inflate.btn_sms.setOnClickListener {
            inflate.findNavController().navigate(R.id.action_homeFragment_to_smsPaketFragment)
        }
        inflate.view_phone.setOnClickListener {
            inflate.findNavController().navigate(R.id.action_homeFragment_to_minutPaketFragment)
        }
        inflate.btn_card.setOnClickListener {
            inflate.findNavController().navigate(R.id.action_homeFragment_to_tarifFragment2)
        }
        inflate.btn_slider.setOnClickListener {
            inflate.findNavController().navigate(R.id.action_homeFragment_to_xizmatFragment)
        }
        inflate.btn_hash.setOnClickListener {
            inflate.findNavController().navigate(R.id.action_homeFragment_to_USSDFragment)
        }
        return inflate
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment HomeFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: Tarif) =
            HomeFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(ARG_PARAM1, param1)
                }
            }
    }
}