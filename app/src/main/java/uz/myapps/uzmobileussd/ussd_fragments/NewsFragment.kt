package uz.myapps.uzmobileussd.ussd_fragments

import android.os.Build
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ExpandableListView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.fragment_news.view.*
import uz.myapps.uzmobileussd.R
import uz.myapps.uzmobileussd.adapters.ExpandableAdapter
import uz.myapps.uzmobileussd.adapters.ExpandableNewsAdapter
import uz.myapps.uzmobileussd.models.News
import uz.myapps.uzmobileussd.models.Xizmat
import uz.myapps.uzmobileussd.viewmodels.UzmViewModel

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class NewsFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var list = ArrayList<News>()
    private val TAG = "NewsFragment"
    private lateinit var adapter: ExpandableNewsAdapter
    private lateinit var ev: ExpandableListView
    lateinit var uzmViewModel: UzmViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val inflate = inflater.inflate(R.layout.fragment_news, container, false)
        ev = inflate.findViewById<ExpandableListView>(R.id.expand_news)
        if (list.size == 0) {
            getData()
        }
        //ikkinchisini ochganda birinchisini yopish
        ev.setOnGroupExpandListener(object : ExpandableListView.OnGroupExpandListener {
            var previousGroup: Int = -1
            override fun onGroupExpand(p0: Int) {
                if (p0 != previousGroup)
                    ev.collapseGroup(previousGroup);
                previousGroup = p0;
            }
        })
        //indicatorni o'ng tomonga joylash
        val metrics = DisplayMetrics()
        requireActivity().windowManager.getDefaultDisplay().getMetrics(metrics)
        val width = metrics.widthPixels
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) {
            ev.setIndicatorBounds(
                width - GetPixelFromDips(50f),
                width - GetPixelFromDips(10f)
            )

        } else {
            ev.setIndicatorBoundsRelative(
                width - GetPixelFromDips(50f),
                width - GetPixelFromDips(10f)
            )
        }
        inflate.back_news.setOnClickListener {
            fragmentManager?.popBackStack()
        }
        return inflate
    }

    private fun getData() {
        uzmViewModel = ViewModelProviders.of(this)[UzmViewModel::class.java]
        uzmViewModel.getNewsData(requireContext()).observe(requireActivity(), Observer {
            adapter = ExpandableNewsAdapter(requireContext(), it)
            ev.setAdapter(adapter)
        })
    }

    fun GetPixelFromDips(pixels: Float): Int {
        // Get the screen's density scale
        val scale = resources.displayMetrics.density
        // Convert the dps to pixels, based on density scale
        return (pixels * scale + 0.5f).toInt()
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment NewsFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            NewsFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}