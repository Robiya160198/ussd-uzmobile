package uz.myapps.uzmobileussd.ussd_fragments

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.preference.PreferenceManager
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentManager
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.fragment_internet_paket.view.*
import kotlinx.android.synthetic.main.fragment_minut_paket.view.*
import uz.myapps.uzmobileussd.R
import uz.myapps.uzmobileussd.adapters.NetPageViewAdapter
import java.net.URLEncoder

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [InternetPaketFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class InternetPaketFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var list = ArrayList<String>()
    var lang: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val inflate = inflater.inflate(R.layout.fragment_internet_paket, container, false)
        val preference =
            PreferenceManager.getDefaultSharedPreferences(requireContext())
        lang = preference.getString("Language", "null")
        load()
        val adapter = NetPageViewAdapter(list, requireFragmentManager())
        inflate.view_pager_net.adapter = adapter
        inflate.tablayout_net.setupWithViewPager(inflate.view_pager_net)
        setupTabs(inflate)
        val tabLayout = inflate.findViewById(R.id.tablayout_net) as TabLayout
        val betweenSpace = 30
        val slidingTabStrip = tabLayout.getChildAt(0) as ViewGroup
        for (i in 0 until slidingTabStrip.childCount - 1) {
            val v = slidingTabStrip.getChildAt(i)
            val params = v.layoutParams as ViewGroup.MarginLayoutParams
            params.rightMargin = betweenSpace
        }
        inflate.back_net.setOnClickListener {
            activity?.onBackPressed()
        }
        inflate.checkup_net.setOnClickListener {
            setupPermissions()
        }
        return inflate
    }

    private fun setupPermissions() {
        if (ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.CALL_PHONE
            )
            != PackageManager.PERMISSION_GRANTED
        ) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    requireActivity(),
                    Manifest.permission.CALL_PHONE
                )
            ) {
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(
                    requireActivity(),
                    arrayOf(Manifest.permission.CALL_PHONE),
                    42
                )
            }
        } else {
            // Permission has already been granted
            callPhone()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 42) {
            // If request is cancelled, the result arrays are empty.
            if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                // permission was granted, yay!
                callPhone()
            } else {
            }
            return
        }
    }

    private fun callPhone() {
        val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel:" + URLEncoder.encode("*107#")))
        startActivity(intent)
    }

    private fun setupTabs(
        view: View
    ) {
        val childCount = view.tablayout_net.tabCount
        for (i in 0 until childCount) {
            val tab = view.tablayout_net.getTabAt(i)
            val tabView = layoutInflater.inflate(
                R.layout.tab_item,
                null,
                false
            )
            val tv = tabView.findViewById<TextView>(R.id.tv)
            val linear = tabView.findViewById<LinearLayout>(R.id.linear)
            tv.text = list[i]
            tab?.customView = tabView
        }
    }

    fun load() {
        when (lang) {
            "ru" -> {
                list.add("Ежемесячные пакеты")
                list.add("Ежедневные пакеты")
                list.add("Ночной интернет")
                list.add("Интернет-пакеты для TAS-IX")
                list.add("Интернет без остановок")
            }
            "uz" -> {
                list.add("Ойлик пакетлар")
                list.add("Ежедневные пакеты")
                list.add("Ночной интернет")
                list.add("Интернет-пакеты для TAS-IX")
                list.add("Интернет без остановок")
            }
            else -> {
                list.add("Oylik paketlar")
                list.add("Кунлик пакетлар")
                list.add("Тунги интернет")
                list.add("ТАС-ИХ учун интернет пакетлар")
                list.add("Интернет нон-стоп")
            }
        }

    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment InternetPaketFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            InternetPaketFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}