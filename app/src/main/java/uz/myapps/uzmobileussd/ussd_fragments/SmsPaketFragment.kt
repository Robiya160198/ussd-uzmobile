package uz.myapps.uzmobileussd.ussd_fragments

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.MarginLayoutParams
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.fragment_internet_paket.view.*
import kotlinx.android.synthetic.main.fragment_internet_paket.view.tablayout_net
import kotlinx.android.synthetic.main.fragment_internet_paket.view.view_pager_net
import kotlinx.android.synthetic.main.fragment_sms_paket.view.*
import uz.myapps.uzmobileussd.R
import uz.myapps.uzmobileussd.adapters.SMSPageViewAdapter
import java.net.URLEncoder


class SmsPaketFragment : Fragment() {
    private var list = ArrayList<String>()
    var lang:String?=null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val inflate = inflater.inflate(R.layout.fragment_sms_paket, container, false)
        val preference =
            PreferenceManager.getDefaultSharedPreferences(requireContext())
        lang = preference.getString("Language", "null")
        load()
        val adapter = SMSPageViewAdapter(list, requireFragmentManager())
        inflate.view_pager_net.adapter = adapter
        inflate.tablayout_net.setupWithViewPager(inflate.view_pager_net)
        setupTabs(inflate)
        val tabLayout = inflate.findViewById(R.id.tablayout_net) as TabLayout
        val betweenSpace = 30
        val slidingTabStrip = tabLayout.getChildAt(0) as ViewGroup
        for (i in 0 until slidingTabStrip.childCount - 1) {
            val v = slidingTabStrip.getChildAt(i)
            val params = v.layoutParams as MarginLayoutParams
            params.rightMargin = betweenSpace
        }
        inflate.back_sms.setOnClickListener {
            activity?.onBackPressed()
        }
        inflate.checkup_sms.setOnClickListener {
            setupPermissions()
        }
        return inflate
    }
    private fun setupTabs(
        view: View
    ) {
        val childCount = view.tablayout_net.tabCount
        for (i in 0 until childCount) {
            val tab = view.tablayout_net.getTabAt(i)
            val tabView = layoutInflater.inflate(
                R.layout.tab_item,
                null,
                false
            )
            val tv = tabView.findViewById<TextView>(R.id.tv)
            val linear = tabView.findViewById<LinearLayout>(R.id.linear)
            tv.text = list[i]
            tab?.customView = tabView
        }
    }
    private fun setupPermissions() {
        if (ContextCompat.checkSelfPermission(requireContext(),
                Manifest.permission.CALL_PHONE)
            != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(requireActivity(),
                    Manifest.permission.CALL_PHONE)) {
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(requireActivity(),
                    arrayOf(Manifest.permission.CALL_PHONE),
                    42)
            }
        } else {
            // Permission has already been granted
            callPhone()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 42) {
            // If request is cancelled, the result arrays are empty.
            if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                // permission was granted, yay!
                callPhone()
            } else {
            }
            return
        }
    }
    private fun callPhone() {
        val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel:" + URLEncoder.encode("*107#")))
        startActivity(intent)
    }

    fun load() {
        when(lang){
            "ru"->{
                list.add("Ежедневные SMS-пакеты")
                list.add("Ежемесячные SMS-пакеты")
                list.add("Международные SMS-пакеты")
            }
            "uz"->{
                list.add("Кунлик СМС пакетлар")
                list.add("Ойлик СМС пакетлар")
                list.add("Халқаро СМС пакетлар")
            }
            else->{
                list.add("Kunlik SMS paketlar")
                list.add("Oylik SMS paketlar")
                list.add("Xalqaro SMS paketlar")
            }
        }
    }
}