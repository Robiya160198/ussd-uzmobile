package uz.myapps.uzmobileussd.ussd_fragments

import android.os.Build
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ExpandableListView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.fragment_operator.view.*
import uz.myapps.uzmobileussd.R
import uz.myapps.uzmobileussd.adapters.ExpandableAdapter
import uz.myapps.uzmobileussd.adapters.RvAloqaAdapter
import uz.myapps.uzmobileussd.models.Aloqa
import uz.myapps.uzmobileussd.models.Xizmat
import uz.myapps.uzmobileussd.viewmodels.UzmViewModel

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [OperatorFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class OperatorFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private var list = ArrayList<Aloqa>()
    private val TAG = "XizmatFragment"
    private lateinit var adapter: RvAloqaAdapter
    private lateinit var ev: RecyclerView
    lateinit var uzmViewModel: UzmViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val inflate = inflater.inflate(R.layout.fragment_operator, container, false)
        ev = inflate.findViewById<RecyclerView>(R.id.rv_aloqa)
        if (list.size == 0) {
            getData()
        }
        inflate.back_operator.setOnClickListener {
            fragmentManager?.popBackStack()
        }
        return inflate
    }
    private fun getData() {
        uzmViewModel = ViewModelProviders.of(this)[UzmViewModel::class.java]
        uzmViewModel.getAloqaData(requireContext()).observe(requireActivity(), Observer {
            adapter = RvAloqaAdapter(it)
            ev.setAdapter(adapter)
        })

    }

    companion object {

        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            OperatorFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}