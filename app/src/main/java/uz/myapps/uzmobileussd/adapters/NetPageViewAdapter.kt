package uz.myapps.uzmobileussd.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.fragment.app.FragmentStatePagerAdapter
import uz.myapps.uzmobileussd.models.Paket
import uz.myapps.uzmobileussd.models.Tarif
import uz.myapps.uzmobileussd.ussd_fragments.viewpagerfragments.PageViewBannerFragment
import uz.myapps.uzmobileussd.ussd_fragments.viewpagerfragments.PageViewNetFragment

@Suppress("DEPRECATION")
class NetPageViewAdapter(var list: ArrayList<String>, fm: FragmentManager) :
    FragmentStatePagerAdapter(fm) {
    override fun getItem(position: Int): Fragment {
        return PageViewNetFragment.newInstance(list[position])
    }

    override fun getCount(): Int {
        return list.size
    }
}