package uz.myapps.uzmobileussd.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import uz.myapps.uzmobileussd.models.Paket
import uz.myapps.uzmobileussd.models.Tarif
import uz.myapps.uzmobileussd.ussd_fragments.viewpagerfragments.PageViewBannerFragment
import uz.myapps.uzmobileussd.ussd_fragments.viewpagerfragments.PageViewNetFragment
import uz.myapps.uzmobileussd.ussd_fragments.viewpagerfragments.PageViewSMSFragment

class SMSPageViewAdapter(var list: ArrayList<String>, fm: FragmentManager) :
    FragmentPagerAdapter(fm) {
    override fun getItem(position: Int): Fragment {
        return PageViewSMSFragment.newInstance(list[position])
    }

    override fun getCount(): Int {
        return list.size
    }
}