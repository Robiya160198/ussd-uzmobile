package uz.myapps.uzmobileussd.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_rv_aloqa.view.*
import kotlinx.android.synthetic.main.item_rv_net.view.*
import kotlinx.android.synthetic.main.item_rv_net.view.name
import uz.myapps.uzmobileussd.R
import uz.myapps.uzmobileussd.models.Aloqa
import uz.myapps.uzmobileussd.models.Paket

class RvAloqaAdapter(var list: List<Aloqa>) : RecyclerView.Adapter<RvAloqaAdapter.VH>() {
    inner class VH(view: View) : RecyclerView.ViewHolder(view) {
        fun onBind(paket: Aloqa) {
            itemView.name.text = paket.name
            itemView.title.text = paket.title
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        val inflate =
            LayoutInflater.from(parent?.context).inflate(R.layout.item_rv_aloqa, parent, false)
        return VH(inflate)
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.onBind(list[position])
    }
}