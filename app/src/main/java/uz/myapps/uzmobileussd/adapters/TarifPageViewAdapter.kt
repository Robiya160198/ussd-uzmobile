package uz.myapps.uzmobileussd.adapters

import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.navigation.Navigation
import uz.myapps.uzmobileussd.R
import uz.myapps.uzmobileussd.models.Tarif
import uz.myapps.uzmobileussd.ussd_fragments.BannerFragment
import uz.myapps.uzmobileussd.ussd_fragments.viewpagerfragments.PageViewTarifFragment

class TarifPageViewAdapter(var root: View, var list: List<Tarif>, fm: FragmentManager) :
    FragmentStatePagerAdapter(fm) {
    override fun getItem(position: Int): Fragment {
        return PageViewTarifFragment.newInstance(list[position],object: onClick{
            override fun onclick() {
                val bundleOf = bundleOf("position" to position)
                Navigation.findNavController(this@TarifPageViewAdapter.root).navigate(R.id.bannerFragment,bundleOf)
            }
        })
    }

    override fun getCount(): Int {
        return list.size
    }

    interface onClick{
        fun onclick()
    }
}