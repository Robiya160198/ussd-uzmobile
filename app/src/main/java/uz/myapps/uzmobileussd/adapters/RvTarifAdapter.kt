package uz.myapps.uzmobileussd.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_rv_net.view.name
import kotlinx.android.synthetic.main.item_rv_net.view.price
import kotlinx.android.synthetic.main.item_rv_net.view.trafik
import kotlinx.android.synthetic.main.item_rv_tarif.view.*
import uz.myapps.uzmobileussd.R
import uz.myapps.uzmobileussd.models.Tarif

class RvTarifAdapter(var list: List<Tarif>,var onclick:onClick) : RecyclerView.Adapter<RvTarifAdapter.VH>() {
    inner class VH(view: View) : RecyclerView.ViewHolder(view) {
        fun onBind(tarif: Tarif, position: Int) {
            itemView.sms.text = tarif.sms
            itemView.name.text = tarif.name
            itemView.daqiqa.text = tarif.minuts
            itemView.trafik.text = tarif.mb
            itemView.price.text = tarif.money
            itemView.setOnClickListener {
                onclick.onclick(position)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        val inflate =
            LayoutInflater.from(parent?.context).inflate(R.layout.item_rv_tarif, parent, false)
        return VH(inflate)
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.onBind(list[position],position)
    }
    interface onClick{
        fun onclick(position: Int)
    }
}