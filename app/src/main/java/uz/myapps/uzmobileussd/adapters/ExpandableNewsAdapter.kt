package uz.myapps.uzmobileussd.adapters

import android.animation.ObjectAnimator
import android.content.Context
import android.util.SparseBooleanArray
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import kotlinx.android.synthetic.main.item_child.view.*
import kotlinx.android.synthetic.main.item_ev_news.view.*
import kotlinx.android.synthetic.main.item_ev_xizmat.view.*
import kotlinx.android.synthetic.main.item_ev_xizmat.view.name
import uz.myapps.uzmobileussd.R
import uz.myapps.uzmobileussd.models.News
import uz.myapps.uzmobileussd.models.Paket
import uz.myapps.uzmobileussd.models.Xizmat


class ExpandableNewsAdapter(var context: Context, var listTitles: List<News>) :
    BaseExpandableListAdapter() {

    override fun getGroup(p0: Int): Any = listTitles[p0]

    override fun isChildSelectable(p0: Int, p1: Int): Boolean = true

    override fun hasStableIds(): Boolean = true

    override fun getGroupView(p0: Int, p1: Boolean, p2: View?, p3: ViewGroup?): View {
        val view = LayoutInflater.from(context).inflate(R.layout.item_ev_news, p3, false)
        view.name.text=listTitles[p0].name
        view.time.text=listTitles[p0].time
        return view
    }

    override fun getChildrenCount(p0: Int): Int = 1

    override fun getChild(p0: Int, p1: Int) = listTitles[p0]

    override fun getGroupId(p0: Int): Long = p0.toLong()

    override fun getChildView(p0: Int, p1: Int, p2: Boolean, p3: View?, p4: ViewGroup?): View {
        val view = LayoutInflater.from(context).inflate(R.layout.item_child_news, p4, false)
        view.about.text=getChild(p0,p1).about.toString()
        return view
    }

    override fun getChildId(p0: Int, p1: Int): Long = p1.toLong()

    override fun getGroupCount(): Int = listTitles.size
}