package uz.myapps.uzmobileussd.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.fragment.app.FragmentStatePagerAdapter
import uz.myapps.uzmobileussd.models.Tarif
import uz.myapps.uzmobileussd.ussd_fragments.viewpagerfragments.PageViewBannerFragment

class BannerPageViewAdapter(var list: List<Tarif>, fm: FragmentManager) :
    FragmentStatePagerAdapter(fm) {
    override fun getItem(position: Int): Fragment {
        return PageViewBannerFragment.newInstance(list[position])
    }

    override fun getCount(): Int {
        return list.size
    }
}