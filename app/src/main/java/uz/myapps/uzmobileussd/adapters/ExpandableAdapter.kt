package uz.myapps.uzmobileussd.adapters

import android.content.Context
import android.preference.PreferenceManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import kotlinx.android.synthetic.main.item_child.view.*
import kotlinx.android.synthetic.main.item_ev_xizmat.view.*
import uz.myapps.uzmobileussd.R
import uz.myapps.uzmobileussd.models.Xizmat


class ExpandableAdapter(var context: Context, var listTitles: List<Xizmat>,var ulanish:Ulanish) :
    BaseExpandableListAdapter() {

    override fun getGroup(p0: Int): Any = listTitles[p0]

    override fun isChildSelectable(p0: Int, p1: Int): Boolean = true

    override fun hasStableIds(): Boolean = true

    override fun getGroupView(p0: Int, p1: Boolean, p2: View?, p3: ViewGroup?): View {
        val view = LayoutInflater.from(context).inflate(R.layout.item_ev_xizmat, p3, false)
        view.name.text=listTitles[p0].name
        return view
    }

    override fun getChildrenCount(p0: Int): Int = 1

    override fun getChild(p0: Int, p1: Int) = listTitles[p0]

    override fun getGroupId(p0: Int): Long = p0.toLong()

    override fun getChildView(p0: Int, p1: Int, p2: Boolean, p3: View?, p4: ViewGroup?): View {
        val view = LayoutInflater.from(context).inflate(R.layout.item_child, p4, false)
        view.about.text=getChild(p0,p1).about.toString()
        val preference =
            PreferenceManager.getDefaultSharedPreferences(context)
        val lang = preference.getString("Language", "null")
        when(lang){
            "ru"->{
                view.activeCod.text="Активация - "+getChild(p0,p1).activeCod.toString()
                if(getChild(p0,p1).passivCod!=null)
                    view.passivCod.text="Деактивировать услугу - "+getChild(p0,p1).passivCod.toString()
                else
                    view.passivCod.visibility=View.GONE
                view.btn_ulanish.setOnClickListener {
                    ulanish.Ulanish(getChild(p0,p1).activeCod.toString())
                }
            }
            "uz"->{
                view.activeCod.text="Фаоллаштириш - "+getChild(p0,p1).activeCod.toString()
                if(getChild(p0,p1).passivCod!=null)
                    view.passivCod.text="Хизматни ўчириш - "+getChild(p0,p1).passivCod.toString()
                else
                    view.passivCod.visibility=View.GONE
                view.btn_ulanish.setOnClickListener {
                    ulanish.Ulanish(getChild(p0,p1).activeCod.toString())
                }
            }
            else->{
                view.activeCod.text="Faollashtirish - "+getChild(p0,p1).activeCod.toString()
                if(getChild(p0,p1).passivCod!=null)
                    view.passivCod.text="Xizmatni o'chirish - "+getChild(p0,p1).passivCod.toString()
                else
                    view.passivCod.visibility=View.GONE
                view.btn_ulanish.setOnClickListener {
                    ulanish.Ulanish(getChild(p0,p1).activeCod.toString())
                }
            }
        }
        return view
    }

    override fun getChildId(p0: Int, p1: Int): Long = p1.toLong()

    override fun getGroupCount(): Int = listTitles.size
    interface Ulanish{
        fun Ulanish(activeCod: String)
    }
}