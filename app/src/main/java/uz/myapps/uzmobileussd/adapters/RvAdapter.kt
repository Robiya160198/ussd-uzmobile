package uz.myapps.uzmobileussd.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_rv_net.view.*
import uz.myapps.uzmobileussd.R
import uz.myapps.uzmobileussd.models.Paket

class RvAdapter(var list: List<Paket>) : RecyclerView.Adapter<RvAdapter.VH>() {
    inner class VH(view: View) : RecyclerView.ViewHolder(view) {
        fun onBind(paket: Paket) {
            itemView.amount.text = paket.amount.toString()
            itemView.name.text = paket.name
            itemView.price.text = paket.price
            itemView.trafik.text = paket.amount.toString()
            itemView.time.text = paket.time
            itemView.activeCod.text = paket.activeCod
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        val inflate =
            LayoutInflater.from(parent?.context).inflate(R.layout.item_rv_net, parent, false)
        return VH(inflate)
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.onBind(list[position])
    }
}