package uz.myapps.uzmobileussd.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import uz.myapps.uzmobileussd.ussd_fragments.viewpagerfragments.PageViewMinutFragment
import uz.myapps.uzmobileussd.ussd_fragments.viewpagerfragments.PageViewSMSFragment

class MinutPageViewAdapter(var list: ArrayList<String>, fm: FragmentManager) :
    FragmentPagerAdapter(fm) {
    override fun getItem(position: Int): Fragment {
        return PageViewMinutFragment.newInstance(list[position])
    }

    override fun getCount(): Int {
        return list.size
    }
}